from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, TextAreaField, SubmitField, \
    SelectField, FieldList
from wtforms.validators import DataRequired


class WelcomeForm(FlaskForm):
    submit_tocheck = SubmitField('Check existing files')
    submit_tocreate = SubmitField('Create a new entry')

class CreateForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired()])
    responsible = StringField('Responsible Person(s)', validators=[DataRequired()])
    more_resp = FieldList(StringField())
    remember_me = BooleanField('Remember Me')
    category = SelectField('Entry Category')
    description = TextAreaField('Description', validators=[DataRequired()])
    projectname = StringField('Project name', validators=[DataRequired()])
    entryname = StringField('Entry name', validators=[DataRequired()])
    submit = SubmitField('Submit')

class AddDataForm(FlaskForm):
    submit = SubmitField('Load data')

class ValidationForm(FlaskForm):
    directory = StringField('Path to be checked (default is current folder)')
    exclude = FieldList(StringField())
    submit = SubmitField(label='Check file structure')
    single_dir = StringField('Path to parent directory')
    single_entry = StringField('Entry to be checked (3 Folders)')
    submit_single = SubmitField(label='Check single entry')

