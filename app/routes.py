import os

from app import app
from flask import render_template, redirect, url_for, request, jsonify, flash
from werkzeug.utils import secure_filename

# get Categories of filesystem from categories.txt
# categories into a dictionary and a list of tuples
CATEGORIES = []
cat_abbrev = []
file = open("app/categories.txt", 'r')
for line in file: 
    if line.rstrip() == "": 
        continue         
    CATEGORIES.append(line.rstrip())
    cat_abbrev.append(line[:3]) 

cat_dict = {let: cat for let, cat in zip(cat_abbrev, CATEGORIES)}
cat_list = [(let, cat) for let, cat in zip(cat_abbrev, CATEGORIES)]

# set here the root path of the file system reading from file_system.txt
file = open("app/file_system.txt", "r") 
fs_root=str(file.readline())
if "enter/file_system/root/directory/here" in fs_root:
    print(33*"-"+"\nATTENTION: no filesystem root directory set (setting default: ./)")
    fs_root="./"
file.close()

# routes to functions in app

from app.forms import WelcomeForm


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def hello():
    form = WelcomeForm()
    welcome = "Welcome dear user. Here you can choose between checking an existing file system and creating a new one."
    if "submit_tocheck" in request.form and form.submit_tocheck.validate(form):
        return redirect(url_for('check'))
    elif "submit_tocreate" in request.form and form.submit_tocreate.validate(form):
        return redirect(url_for("new_entry"))
    return render_template("welcome.html", title="Home", message=welcome, form=form)


# -----------------------------------------------------------------------------

# function to create a new Entry =============================================
from app.forms import CreateForm


@app.route('/new_entry', methods=['GET', 'POST'])
def new_entry():
    """
    Create a new Database entry with a minimal README.md file
    :return:
    - creates a new DB entry with minimal required README.md
    - links to a page where the upload of data is possible
    """
    form = CreateForm()
    form.category.choices = cat_list
    if "submit" in request.form and form.submit.validate(form):
        # first check correctness of project and entry names
        projectpath = os.path.join(fs_root, cat_dict[form.category.data],
                                   form.projectname.data)
        entrypath = os.path.join(projectpath, form.entryname.data)
        
        # think about a checking system for the directory names
        # add the possibility to select more emails
        
        if not os.path.isdir(projectpath):
            os.mkdir(projectpath)
        os.mkdir(entrypath)
        resp = ", ".join([form.responsible.data] + form.more_resp.data)
        readme = ("---"
                  "\nresponsible: {}"
                  "\nemail: {}"
                  "\ndescription: {}"
                  "\n...").format(resp, form.email.data, form.description.data)
        rmf = open(entrypath + "/README.md", "a")
        rmf.write(readme)
        rmf.close()
        # add the possibility for "freestyle" fields/entries of r.md
        return redirect(url_for('add_data', entry_path=entrypath))
    return render_template("new_entry.html", title="Create new entry", form=form)


from app.forms import AddDataForm


@app.route('/add_data', methods=['GET', 'POST'])
def add_data():
    """
    Upload new data into created entry
    :return:
    """
    # read path and README.md file to display
    entry_path = request.args['entry_path']
    print(entry_path)
    # to pass values from previous view use a session
    form = AddDataForm()
    # change this to the entry path from the new_entry fct!

    if "submit" in request.form and form.submit.validate(form):
        file = request.files['uploadname']
        print(file)
        filename = file.filename
        if file.filename == '':
            flash('No file selected')
            return redirect(request.url)
        if file:  # and allowed_file(file.filename): # need a fct to check extension
            filename = secure_filename(file.filename)
            zippath = os.path.join(entry_path, filename)
            file.save(zippath)
            import zipfile
            with zipfile.ZipFile(zippath, 'r') as zip_ref:
                zip_ref.extractall(entry_path)

        # return redirect(url_for('new_entry'))

    return render_template("add-data.html", title="Add data to entry",
                           form=form, entry=entry_path)


# -----------------------------------------------------------------------------

# function for checking button ================================================

from app.forms import ValidationForm


@app.route('/check', methods=['GET', 'POST'])
def check():
    from check_sfs import check_sfs, check_dir
    form = ValidationForm()

    if "submit" in request.form and form.submit.validate(form):

        # no directory entry checks the cwd
        if form.directory.data == "":
            checking_dir = "./"
        else:
            checking_dir = form.directory.data
        return render_results(form=form,
                              checked_list=check_sfs(checking_dir,
                                                     exclude=form.exclude.data))
    elif "submit_single" in request.form and form.submit_single.validate(form):
        checking_dir = str(form.single_dir.data)
        entry = str(form.single_entry.data)
        return render_results(form=form, checked_list=check_dir(checking_dir, entry))

    return render_template("check.html", title="Check Filesystem", form=form)


def render_results(form, checked_list):
    # this catches single dir tests and file systems with one entry only
    if type(checked_list) == tuple:
        checked_list = [checked_list]
    # populate the messages from the results
    if checked_list != []:
        positive, negative, negative_error = [], [], []
        for out in checked_list:
            if out[-1] == []:
                positive.append(str(out[0]))
            else:
                negative.append(out[0])
                negative_error.append(out[-1])
        form.submit.label.text = "Check again"
        return render_template("check_result.html", title="Result of Check", form=form,
                               pos=positive, neg=negative, negerr=negative_error)
    else:
        return redirect(url_for('check'))


# background functions for ajax requests =============================================

@app.route('/expand-existingdirs')
def existing_dirs_list():
    try:
        category = request.args['category']
        dirs = os.listdir(os.path.join(fs_root, cat_dict[category]))
        return jsonify(existing_dirs=dirs)
    except Exception as e:
        return str(e)
