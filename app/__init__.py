from flask import Flask
from config import Config
from check_sfs import CATEGORIES

app = Flask(__name__)
app.config.from_object(Config)
app.static_folder = 'static'

from app import routes
