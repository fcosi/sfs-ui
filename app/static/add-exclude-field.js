$(document).ready(function() {
  /*
  this function adds an input field to form.exclude upon pressinf on addNewField btn
  the entries are then passed to the check_sfs fct as eclude paths
  */
  var fieldNum = 0; 
    $("#addNewField").click(function() {
    // the if can be handled better
	if ($("#addNewField")[0].innerText == "+"){
	    var idString = "more_resp-";
	    var inputfield =  $("#more_resp");
	    var placeholder = 'further responsible person';
	    var size = 24;}
	else {
	    var idString = "exclude-";
	    var inputfield = $("#exclude");
	    var placeholder = 'exclude directory';
	    var size = 64;}
	var newInput = $("<input type='text' placeholder='"+placeholder+"' size="+size+"></input></br>")
        .attr("class", "form-control") 
        .attr("id", idString + fieldNum) 
        .attr("name", idString + fieldNum) 
        .attr("rows", "1")

	$(inputfield).append(newInput);

    fieldNum++; 
    });
});
