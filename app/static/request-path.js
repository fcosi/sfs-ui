$(document).ready(function() {
    /* Expands dropdown-menu upon click to list the available 
       Project directories
    */
    $("#category").on("click", function() {
	$.getJSON('/expand-existingdirs',
		  {category : $("#category")[0].value},
      function(data){
    $("#parent_dirs").text(" ");
    var message = "<p><u>Existing projects</u>:<br>";
    for(var i=0; i<data.existing_dirs.length; i++){
      message = message + data.existing_dirs[i] + "<br>";
    };
		    	$("#parent_dirs").append(message+"</p>");
  })
	return false;
    })
})
