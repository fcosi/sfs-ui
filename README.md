# SFS UI

Browser tool for checking and creating entries of a standard file structure (sfs)

This UI has the following dependencies:

https://gitlab.com/salexan/check-sfs

https://gitlab.com/salexan/yaml-header-tools


# Requirements

[check_sfs](https://gitlab.com/salexan/check-sfs)

[yaml-header-tools](https://gitlab.com/salexan/yaml-header-tools)

Flask

FlaskWTF

# Usage

## Preliminary steps

To run the app, first some variables of the Filesystem, to which it
is related have to be set. The main categories of the Filesystem have to
be listed in `app/categories.txt`, while the root directory of the
Filesystem has to be stated in `app/file_system.txt`.

## Running the web-server

To run the service on `127.0.0.1` port `5000` (default) simply use:

```bash
flask run
```

or

```bash
python main.py
```

## Running the web-server on a different host

To change the host and/or the port of the web-server (as e.g. to
deploy it on a local machine), change the corresponding values in
`main.py` or run the server with

```bash
flask run --host 0.0.0.0 --port 8000
```

here `0.0.0.0` and `8000` are placeholders for the new adress and the
new port.

# Contributers

The original authors of this package are:

- Filippo G. Cosi
- Alexander Schlemmer
- Florian Spreckelsen


# License

Copyright (C) 2020 Research Group Biomedical Physics, Max Planck Institute for
Dynamics and Self-Organization Göttingen.

All files in this repository are licensed under a [GNU Affero General Public
License](LICENSE) (version 3 or later).
